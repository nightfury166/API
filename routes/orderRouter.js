'use strict';
module.exports = function(app) {
   var todoList = require('../controllers/orderController');

   // todoList Routes
   app.route('/orders')
       .get(todoList.list_all_order)
       .post(todoList.create_a_order);

   app.route('/orders/:orderId')
       .get(todoList.read_a_order)
       .put(todoList.update_a_order)
       .delete(todoList.delete_a_order);
};
