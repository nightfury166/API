'user strict';
var sql = require('./db.js');

var Customer = function(customer){
    this.name = customer.name;
    this.phone = customer.phone;
    this.email = customer.email;
    this.address = customer.address;
    this.created_at = new Date();
};
Customer.createCustomer = function createCustomer(newCustomer, result) {
    sql.query("INSERT INTO customers set ?", newCustomer, function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Customer.geCustomerById = function geCustomerById(customerId, result) {
    sql.query("Select name, phone, email, address from customers where id = ?", customerId, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);

        }
    });
};
Customer.getAllCustomer = function getAllCustomer(result) {
    sql.query("Select * from customers", function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('customers : ', res);

            result(null, res);
        }
    });
};
Customer.updatCustomerById = function(id, name, phone, email, address , result){
    sql.query("UPDATE customers SET name = ?, phone = ?, email = ?, address = ?  WHERE id = ?", [customer.name, customer.phone, customer.email, customer.address , id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};
Customer.remove = function(id, result){
    sql.query("DELETE FROM customers WHERE id = ?", [id], function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{

            result(null, res);
        }
    });
};

module.exports= Customer;
