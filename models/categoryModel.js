'user strict';
var sql = require('./db.js');

var Category = function(category){
    this.brand_name = category.brand_name;
    this.created_at = new Date();
};
Category.createCategory = function createCategory(newCategory, result) {
    sql.query("INSERT INTO category set ?", newCategory, function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Category.getCategoryById = function getCategoryById(categoryId, result) {
    sql.query("Select brand_name from category where id = ? ", categoryId, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);

        }
    });
};
Category.getAllCategory= function getAllCategory(result) {
    sql.query("Select * from category", function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('category : ', res);

            result(null, res);
        }
    });
};
Category.updateCategoryById = function(id, brand_name, result){
    sql.query("UPDATE category SET brand_name = ? WHERE id = ?", [category.brand_name, id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};
Category.remove = function(id, result){
    sql.query("DELETE FROM category WHERE id = ?", [id], function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{

            result(null, res);
        }
    });
};

module.exports= Category;
