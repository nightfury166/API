'user strict';
var sql = require('./db.js');

//Product object constructor
var Product = function(product){
    this.name = product.name;
    this.category_id = product.category_id;
    this.price = product.price;
    this.description = product.description;
    this.quantity = product.quantity;
    this.sale = product.sale;
    this.image = product.image;
    this.created_at = new Date();
};
Product.createProduct = function createProduct(newProduct, result) {
    sql.query("INSERT INTO products set ?", newProduct, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};
Product.getProductById = function getProductById(productId, result) {
    sql.query("Select * from products where id = ?", productId, function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);

        }
    });
};
Product.getAllProduct = function getAllProduct(result) {
    sql.query("Select * from products ", function (err, res) {

        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};
Product.updateProductById = function(id, name, category_id, price, description,	quantity, sale, image, result){
    sql.query("UPDATE products SET name = ?, category_id = ?, price =?, description = ?, quantity = ?, sale = ?, image = ?  WHERE id = ?", [product.name, product.category_id, product.price, product.description, product.quantity, product.sale, product.image,  id], function (err, res) {
        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};
Product.remove = function(id, result){
    sql.query("DELETE FROM products WHERE id = ?", [id], function (err, res) {

        if(err) {
            result(null, err);
        }
        else{

            result(null, res);
        }
    });
};

module.exports= Product;
