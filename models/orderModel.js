'user strict';
var sql = require('./db.js');

var Order = function(order){
    this.customer_id = order.customer_id;
    this.total_price = order.total_price;
    this.created_at = new Date();
};
Order.createOrder = function createOrder(newOrder, result) {
    sql.query("INSERT INTO orders set ?", newOrder, function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Order.geOrderById = function geOrderById(orderId, result) {
    sql.query("Select customers.name, orders.total_price from orders inner join customers on orders.customer_id =  customers.id where orders.id = ?", orderId, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);

        }
    });
};
Order.getAllOrder = function getAllOrder(result) {
    sql.query("Select customers.name, orders.total_price from orders inner join customers on orders.customer_id =  customers.id", function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('orders : ', res);

            result(null, res);
        }
    });
};
Order.updatOrderById = function(id, customer_id, total_price, result){
    sql.query("UPDATE orders SET customer_id = ?, total_price = ?  WHERE id = ?", [order.customer_id, order.total_price , id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};
Order.remove = function(id, result){
    sql.query("DELETE FROM orders WHERE id = ?", [id], function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{

            result(null, res);
        }
    });
};

module.exports= Order;
