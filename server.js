const express = require('express'),
   app = express(),
   bodyParser = require('body-parser');
port = process.env.PORT || 3000;
app.listen(port);

console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var product = require('./routes/productRoutes'); //importing route
var category = require('./routes/categoryRouter');
var order = require('./routes/orderRouter');
var customer = require('./routes/customerRouter');
product(app); 
category(app);
order(app);
customer(app);