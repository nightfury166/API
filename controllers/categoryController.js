'use strict';

var Category = require('../models/categoryModel.js');

exports.list_all_category = function(req, res) {
    Category.getAllCategory(function(err, category) {
        if (err)
            res.send(err);
        res.send(category);
    });
};

exports.create_a_category = function(req, res) {
    var new_category = new Category(req.body);

    //handles null error
    if(!new_category.brand_name ){

        res.status(400).send({ error:true, message: 'Please provide name/category_id' });

    }
    else{

        Category.createCategory(new_category, function(err, category) {

            if (err)
                res.send(err);
            res.json(category);
        });
    }
};


exports.read_a_category = function(req, res) {
    Category.getCategoryById(req.params.categoryId, function(err, category) {
        if (err)
            res.send(err);
        res.json(category);
    });
};


exports.update_a_category = function(req, res) {
    Category.updateCategoryById(req.params.categoryId, new Category(req.body), function(err, category) {
        if (err)
            res.send(err);
        res.json(category);
    });
};


exports.delete_a_category = function(req, res) {


    Category.remove( req.params.categoryId, function(err, category) {
        if (err)
            res.send(err);
        res.json({ message: 'category successfully deleted' });
    });
};
