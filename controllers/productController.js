'use strict';

var Product = require('../models/productModel.js');

exports.list_all_products = function(req, res) {
    Product.getAllProduct(function(err, product) {
        if (err)
            res.send(err);
        res.send(product);
    });
};

exports.create_a_product = function(req, res) {
    var new_product = new Product(req.body);

    //handles null error
    if(!new_product.name || !new_product.category_id ){

        res.status(400).send({ error:true, message: 'Please provide name/category_id' });

    }
    else{

        Product.createProduct(new_product, function(err, product) {

            if (err)
                res.send(err);
            res.json(product);
        });
    }
};


exports.read_a_product = function(req, res) {
    Product.getProductById(req.params.productId, function(err, product) {
        if (err)
            res.send(err);
        res.json(product);
    });
};


exports.update_a_product = function(req, res) {
    Product.updateProductById(req.params.productId, new Product(req.body), function(err, product) {
        if (err)
            res.send(err);
        res.json(product);
    });
};


exports.delete_a_product = function(req, res) {


    Product.remove( req.params.productId, function(err, product) {
        if (err)
            res.send(err);
        res.json({ message: 'product successfully deleted' });
    });
};
