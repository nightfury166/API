'use strict';

var Order = require('../models/orderModel.js');

exports.list_all_order = function(req, res) {
    Order.getAllOrder(function(err, order) {
        if (err)
            res.send(err);
        res.send(order);
    });
};

exports.create_a_order = function(req, res) {
    var new_order= new Order(req.body);

    //handles null error
    if(!new_order.brand_name ){

        res.status(400).send({ error:true, message: 'Please provide' });

    }
    else{

        Order.createOrder(new_order, function(err, order) {

            if (err)
                res.send(err);
            res.json(order);
        });
    }
};


exports.read_a_order = function(req, res) {
    Order.geOrderById(req.params.orderId, function(err, order) {
        if (err)
            res.send(err);
        res.json(order);
    });
};


exports.update_a_order = function(req, res) {
    Order.updatOrderById(req.params.orderId, new Order(req.body), function(err, order) {
        if (err)
            res.send(err);
        res.json(order);
    });
};


exports.delete_a_order = function(req, res) {


    Order.remove( req.params.orderId, function(err, order) {
        if (err)
            res.send(err);
        res.json({ message: 'successfully deleted' });
    });
};
